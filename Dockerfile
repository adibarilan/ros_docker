FROM ros:melodic
RUN apt update && apt install -y ros-melodic-desktop-full
ENV PYTHONPATH="/opt/ros/melodic/lib/python2.7/dist-packages/:$PYTHONPATH"
